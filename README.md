docker run --name server-2 -p 82:80 -d nginx:1.18-alpine

docker stop server-2

docker ps -a


docker exec -it some-nginx bash

docker logs -f server-2

docker rm -f server-2

docker stop some-nginx
docker rm some-nginx

docker rmi nginx

docker network create app-net

docker run --name mysql-svr -p 3306:3306 \
-e MYSQL_ROOT_PASSWORD=1234 \
-e MYSQL_DATABASE=simple_db \
-e MYSQL_USER=simple \
-e MYSQL_PASSWORD=simple \
--network app-net \
-v /Users/sommaik/Workspace/swpark/docker_proj/dbfile:/var/lib/mysql \
-d mysql:5.7


docker exec -it mysql-svr mysql -uroot -p1234


use simple_db;

create table user( code varchar(10) );

show tables;

exit;



docker run --name myadmin
-d
--link mysql-svr:db
--network app-net
-p 83:80
phpmyadmin/phpmyadmin


docker stop mysql-svr
docker rm mysql-svr

docker run --name mysql-svr \
-e MYSQL_ROOT_PASSWORD=1234 \
-e MYSQL_DATABASE=simple_db \
-e MYSQL_USER=simple \
-e MYSQL_PASSWORD=simple \
--network app-net \
-v /Users/sommaik/Workspace/swpark/docker_proj/dbfile:/var/lib/mysql \
-d mysql:5.7

docker build -t my-web .

docker image ls

docker run --name my-svr -p 82:80 -d my-web

docker run --name dev-svr -p 88:80 \
-v /Users/sommaik/Workspace/swpark/docker_proj/html:/usr/share/nginx/html \
-d nginx:1.18-alpine



docker tag my-web my-web:v1.0

docker build -t my-web:v2.0 .

docker stop my-svr
docker rm my-svr
docker run --name my-svr -p 82:80 -d my-web

docker tag my-web:v2.0 my-web:latest


docker stop mysql-svr myadmin
docker rm mysql-svr myadmin

docker-compose up -d

docker stop todo-api
docker rm todo-api
docker-compose up -d --force-recreate

docker-compose stop myadmin

docker-compose down


docker-machine ls


https://docs.docker.com/machine/install-machine/

docker-machine env --unset

docker tag todo-api:1.0 sommaik/todo-api:1.0

docker login

docker push sommaik/todo-api:1.0


docker-machine env vm1


docker-compose up -d

docker-machine create vm1